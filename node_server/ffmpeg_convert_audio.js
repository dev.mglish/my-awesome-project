var path = require('path');
var ffmpeg = require('fluent-ffmpeg');

ffmpeg.setFfmpegPath(path.join(__dirname, '/ffmpeg/bin/ffmpeg.exe'));
ffmpeg.setFfprobePath(path.join(__dirname, '/ffmpeg/bin/ffprobe.exe'));

var subtitles = [
    {
        start: 5,
        end: 10
    },
    {
        start: 15,
        end: 25
    },
    {
        start: 50,
        end: 110
    }
]

for(var sub of subtitles){
    ffmpeg('./new3.mp4')
    .noVideo()
    .seek(sub.start)
    .duration(sub.end - sub.start)
    .output('./' + sub.start + '_' + sub.end + '.mp3')
    .on('start', ()=>{
      console.log('start processing');
    })
    .on('end', ()=>{
      console.log('finish');
    })
    .run()
}